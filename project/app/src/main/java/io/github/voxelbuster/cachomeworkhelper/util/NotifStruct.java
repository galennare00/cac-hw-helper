package io.github.voxelbuster.cachomeworkhelper.util;

public class NotifStruct {
    private final int quantity;
    private final TimeScale scale;
    private final boolean sticky;
    private final boolean after;
    private final int notifId;

    public enum TimeScale {
        SECONDS,
        MINUTES,
        HOURS,
        DAYS,
        WEEKS,
        MONTHS
    }

    public NotifStruct(int quantity, TimeScale scale, boolean sticky, boolean after, int notifId) {
        this.quantity = quantity;
        this.scale = scale;
        this.sticky = sticky;
        this.after = after;
        this.notifId = notifId;
    }

    public int getQuantity() {
        return quantity;
    }

    public TimeScale getTimeScale() {
        return scale;
    }

    public boolean isStickyNotif() {
        return sticky;
    }

    public boolean isLateNotif() {
        return after;
    }

    public int getNumericId() {
        return notifId;
    }
}
