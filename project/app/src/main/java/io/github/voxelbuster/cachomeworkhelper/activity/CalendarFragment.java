package io.github.voxelbuster.cachomeworkhelper.activity;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

import io.github.voxelbuster.cachomeworkhelper.R;
import io.github.voxelbuster.cachomeworkhelper.util.AssignmentStruct;
import io.github.voxelbuster.cachomeworkhelper.util.DataManager;
import io.github.voxelbuster.cachomeworkhelper.util.NotifStruct;

public class CalendarFragment extends Fragment {
    private View fragmentView;

    private CalendarView calendar_view;
    private ListView calendar_listview;
    private ListView calendar_listview_expand;
    private TextView date_tv;
    private Button create_button;

    private int month = -1, day = -1, year = -1;

    private ArrayAdapter<String> assignAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentView = inflater.inflate(R.layout.calendar_layout, container, false);

        calendar_view = (CalendarView) fragmentView.findViewById(R.id.calendar_view);
        Calendar d = Calendar.getInstance();
        d.setTimeInMillis(calendar_view.getDate());
        month = d.get(Calendar.MONTH);
        day = d.get(Calendar.DAY_OF_MONTH);
        year = d.get(Calendar.YEAR);

        calendar_listview = (ListView) fragmentView.findViewById(R.id.calendar_listview);
        calendar_listview_expand = (ListView) fragmentView.findViewById(R.id.calendar_listview_expand);
        date_tv = (TextView) fragmentView.findViewById(R.id.date_text_view);
        create_button = (Button) fragmentView.findViewById(R.id.calendar_create_button);

        date_tv.setText(new SimpleDateFormat("MM/dd/yyyy").format(calendar_view.getDate()));
        date_tv.setVisibility(View.GONE);

        create_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CalendarFragment.this.getActivity(), CreateAssignmentActivity.class);
                intent.putExtra("date", date_tv.getText());
                CalendarFragment.this.startActivity(intent);
            }
        });

        date_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calendar_view.setVisibility(View.VISIBLE);
                date_tv.setVisibility(View.GONE);
                calendar_listview.setVisibility(View.VISIBLE);
                calendar_listview_expand.setVisibility(View.GONE);
            }
        });

        calendar_view.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                date_tv.setText((month + 1) + "/" + dayOfMonth + "/" + year);
                CalendarFragment.this.month = month;
                CalendarFragment.this.day = dayOfMonth;
                CalendarFragment.this.year = year;
                onResume();
            }
        });

        calendar_listview.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                date_tv.setVisibility(View.VISIBLE);
                calendar_view.setVisibility(View.GONE);
                calendar_listview.setVisibility(View.GONE);
                calendar_listview_expand.setVisibility(View.VISIBLE);
                date_tv.bringToFront();
                return false;
            }
        });

        calendar_listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try {
                    AssignmentStruct assignStruct = new DataManager(CalendarFragment.this.getActivity()).getAssignments()[position];
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    String url = assignStruct.getURI();
                    if (!url.matches("(?:(?:https?|ftp|file):\\/\\/|www\\.|ftp\\.)(?:\\([-A-Z0-9+&@#\\/%=~_|$?!:,.]*\\)|[-A-Z0-9+&@#\\/%=~_|$?!:,.])*(?:\\([-A-Z0-9+&@#\\/%=~_|$?!:,.]*\\)|[A-Z0-9+&@#\\/%=~_|$])")) { // allow users to omit protocol in URLs
                        url = "http://" + url;
                    }
                    intent.setData(Uri.parse(url));
                    getActivity().startActivity(intent);
                } catch (Exception e) {
                    Toast.makeText(CalendarFragment.this.getActivity().getBaseContext(), "Failed to parse assignments.json. If the problem persists, clear your data and try again.", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        });

        calendar_listview.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                PopupMenu popup = new PopupMenu(CalendarFragment.this.getActivity(), view);
                popup.getMenuInflater().inflate(R.menu.assign_popup, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        if (item.getItemId() == R.id.del_assign) {
                            try {
                                new DataManager(CalendarFragment.this.getActivity()).delAssign(CalendarFragment.this.getActivity().getBaseContext(), position);
                                CalendarFragment.this.onResume(); // Refreshes the list
                            } catch (Exception e) {
                                Toast.makeText(CalendarFragment.this.getActivity().getBaseContext(), "Failed to parse assignments.json. If the problem persists, clear your data and try again.", Toast.LENGTH_SHORT).show();
                                e.printStackTrace();
                            }
                        } else if (item.getItemId() == R.id.edit_assign) {
                            try {
                                AssignmentStruct assignStruct = new DataManager(CalendarFragment.this.getActivity()).getAssignments()[position];
                                Intent intent = new Intent(CalendarFragment.this.getActivity().getBaseContext(), CreateAssignmentActivity.class);
                                intent.putExtra("name", assignStruct.getName());
                                intent.putExtra("className", assignStruct.getClassName());
                                intent.putExtra("time", assignStruct.getTime());
                                intent.putExtra("date", assignStruct.getDate());
                                intent.putExtra("uri", assignStruct.getURI());
                                intent.putExtra("nag", assignStruct.isNag());
                                ArrayList<NotifStruct> notifArray = new ArrayList<>(Arrays.asList(assignStruct.getNotifs()));
                                ArrayList<Integer> notifIdArray = new ArrayList<>();
                                for (NotifStruct n : notifArray) {
                                    notifIdArray.add(n.getNumericId());
                                }
                                intent.putIntegerArrayListExtra("notifs", notifIdArray);
                                intent.putExtra("editing", position);
                                CalendarFragment.this.getActivity().startActivity(intent);
                                assignAdapter.remove(assignAdapter.getItem(position));
                            } catch (Exception e) {
                                Toast.makeText(CalendarFragment.this.getActivity().getBaseContext(), "Failed to parse assignments.json. If the problem persists, clear your data and try again.", Toast.LENGTH_SHORT).show();
                                e.printStackTrace();
                            }
                        } else if (item.getItemId() == R.id.comp_assign) {
                            try {
                                DataManager dm = new DataManager(CalendarFragment.this.getActivity());
                                AssignmentStruct assignStruct = dm.getAssignments()[position];
                                if (assignStruct.isCompleted()) return false;
                                AssignmentStruct completed = new AssignmentStruct("✔ " + assignStruct.getName(),
                                        assignStruct.getClassName(),
                                        assignStruct.getDate(),
                                        assignStruct.getTime(),
                                        assignStruct.getURI(),
                                        assignStruct.getNotifs(),
                                        assignStruct.isNag(),
                                        true);
                                assignAdapter.remove(assignAdapter.getItem(position));
                                dm.putAssignment(completed.getName(), completed.getClassName(), completed.getDate(), completed.getTime(),
                                        completed.getURI(), completed.getNotifs(), completed.isNag(), completed.isCompleted());
                                assignAdapter.add(completed.getName());
                                new DataManager(CalendarFragment.this.getActivity()).delAssign(CalendarFragment.this.getActivity().getBaseContext(), position);
                                CalendarFragment.this.onResume();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        return true;
                    }
                });
                popup.show();
                return true;
            }
        });

        calendar_listview_expand.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                PopupMenu popup = new PopupMenu(CalendarFragment.this.getActivity(), view);
                popup.getMenuInflater().inflate(R.menu.assign_popup, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        if (item.getItemId() == R.id.del_assign) {
                            try {
                                new DataManager(CalendarFragment.this.getActivity()).delAssign(CalendarFragment.this.getActivity().getBaseContext(), position);
                                CalendarFragment.this.onResume(); // Refreshes the list
                            } catch (Exception e) {
                                Toast.makeText(CalendarFragment.this.getActivity().getBaseContext(), "Failed to parse assignments.json. If the problem persists, clear your data and try again.", Toast.LENGTH_SHORT).show();
                                e.printStackTrace();
                            }
                        } else if (item.getItemId() == R.id.edit_assign) {
                            try {
                                AssignmentStruct assignStruct = new DataManager(CalendarFragment.this.getActivity()).getAssignments()[position];
                                Intent intent = new Intent(CalendarFragment.this.getActivity().getBaseContext(), CreateAssignmentActivity.class);
                                intent.putExtra("name", assignStruct.getName());
                                intent.putExtra("className", assignStruct.getClassName());
                                intent.putExtra("time", assignStruct.getTime());
                                intent.putExtra("date", assignStruct.getDate());
                                intent.putExtra("uri", assignStruct.getURI());
                                intent.putExtra("nag", assignStruct.isNag());
                                ArrayList<NotifStruct> notifArray = new ArrayList<>(Arrays.asList(assignStruct.getNotifs()));
                                ArrayList<Integer> notifIdArray = new ArrayList<>();
                                for (NotifStruct n : notifArray) {
                                    notifIdArray.add(n.getNumericId());
                                }
                                intent.putIntegerArrayListExtra("notifs", notifIdArray);
                                intent.putExtra("editing", position);
                                CalendarFragment.this.getActivity().startActivity(intent);
                                assignAdapter.remove(assignAdapter.getItem(position));
                            } catch (Exception e) {
                                Toast.makeText(CalendarFragment.this.getActivity().getBaseContext(), "Failed to parse assignments.json. If the problem persists, clear your data and try again.", Toast.LENGTH_SHORT).show();
                                e.printStackTrace();
                            }
                        } else if (item.getItemId() == R.id.comp_assign) {
                            try {
                                DataManager dm = new DataManager(CalendarFragment.this.getActivity());
                                AssignmentStruct assignStruct = dm.getAssignments()[position];
                                if (assignStruct.isCompleted()) return false;
                                AssignmentStruct completed = new AssignmentStruct("✔ " + assignStruct.getName(),
                                        assignStruct.getClassName(),
                                        assignStruct.getDate(),
                                        assignStruct.getTime(),
                                        assignStruct.getURI(),
                                        assignStruct.getNotifs(),
                                        assignStruct.isNag(),
                                        true);
                                assignAdapter.remove(assignAdapter.getItem(position));
                                dm.putAssignment(completed.getName(), completed.getClassName(), completed.getDate(), completed.getTime(),
                                        completed.getURI(), completed.getNotifs(), completed.isNag(), completed.isCompleted());
                                assignAdapter.add(completed.getName());
                                new DataManager(CalendarFragment.this.getActivity()).delAssign(CalendarFragment.this.getActivity().getBaseContext(), position);
                                CalendarFragment.this.onResume();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        return true;
                    }
                });
                popup.show();
                return true;
            }
        });

        return fragmentView;
    }

    @Override
    public void onResume() {
        super.onResume();
        Calendar cal = Calendar.getInstance();
        if (month > -1) {
            cal.set(year, month, day);
        }
        Log.d("DateCheck",new SimpleDateFormat("MM/dd/yyyy").format(cal.getTime()));
        ArrayList<String> names = new ArrayList<>();
        try {
            for (AssignmentStruct c : new DataManager(getActivity()).getAssignments()) {
                Log.d("DateCheck2", c.getDate());
                if (new SimpleDateFormat("MM/dd/yyyy").format(cal.getTime()).equals(c.getDate())) {
                    names.add(c.getName());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        assignAdapter = new ArrayAdapter<String>(fragmentView.getContext(), R.layout.list_view_layout, R.id.lv_text, names);
        calendar_listview.setAdapter(assignAdapter);
        calendar_listview_expand.setAdapter(assignAdapter);
        assignAdapter.notifyDataSetChanged();
    }
}