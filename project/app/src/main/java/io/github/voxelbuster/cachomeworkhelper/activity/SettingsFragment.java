package io.github.voxelbuster.cachomeworkhelper.activity;

import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;

import java.io.File;

import io.github.voxelbuster.cachomeworkhelper.R;
import io.github.voxelbuster.cachomeworkhelper.util.DataManager;

public class SettingsFragment extends Fragment {
    private View fragmentView;

    private Button clear_data_button;
    private Switch dark_theme_sw;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentView = inflater.inflate(R.layout.settings_layout, container, false);
        clear_data_button = (Button) fragmentView.findViewById(R.id.clear_data_button);
        dark_theme_sw = (Switch) fragmentView.findViewById(R.id.dark_theme_sw);
        dark_theme_sw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                } else {}
            }
        });

        clear_data_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage("Delete all user data? This cannot be undone.")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                DataManager.deleteDirectory(new File(getActivity().getFilesDir().getAbsolutePath() + "/userdata"));
                                Toast.makeText(fragmentView.getContext(), "Data deleted", Toast.LENGTH_SHORT).show();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Toast.makeText(fragmentView.getContext(), "Cancelled", Toast.LENGTH_SHORT).show();
                            }
                        });
                builder.create().show();
            }
        });
        return fragmentView;
    }
}