package io.github.voxelbuster.cachomeworkhelper.service;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.media.RingtoneManager;
import android.support.v4.app.NotificationCompat;

import java.util.HashMap;

import io.github.voxelbuster.cachomeworkhelper.activity.MainActivity;

public class NotificationService  { // TODO finish
    public static String NOTIFICATION_ID = "hh_notif";
    public static String NOTIFICATION = "notif";

    public static HashMap<Integer, PendingIntent> pendingNotifs = new HashMap<>();

    public static void scheduleNotification(Context context, long date, int notificationId, String title, String content) {//delay is after how much time(in millis) from current time you want to schedule the notification
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setContentTitle(title)
                .setContentText(content)
                .setAutoCancel(true)
                .setSmallIcon(android.R.drawable.sym_def_app_icon) // TODO change app icon
                .setLargeIcon(((BitmapDrawable) context.getResources().getDrawable(android.R.drawable.sym_def_app_icon)).getBitmap())
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

        Intent intent = new Intent(context, MainActivity.class);
        PendingIntent activity = PendingIntent.getActivity(context, notificationId, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        builder.setContentIntent(activity);

        Notification notification = builder.build();

        Intent notificationIntent = new Intent(context, Reciever.class);
        notificationIntent.putExtra(NOTIFICATION_ID, notificationId);
        notificationIntent.putExtra(NOTIFICATION, notification);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, notificationId, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, date, pendingIntent); // Set the alarm to fire on the set date

        pendingNotifs.put(notificationId, pendingIntent);
    }

    public static void schedulePersistentNotification(Context context, long date, int notificationId, String title, String content) {//delay is after how much time(in millis) from current time you want to schedule the notification
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setContentTitle(title)
                .setContentText(content)
                .setAutoCancel(false)
                .setOngoing(true)
                .setSmallIcon(android.R.drawable.sym_def_app_icon) // TODO change app icon
                .setLargeIcon(((BitmapDrawable) context.getResources().getDrawable(android.R.drawable.sym_def_app_icon)).getBitmap())
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

        Intent intent = new Intent(context, MainActivity.class);
        PendingIntent activity = PendingIntent.getActivity(context, notificationId, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        builder.setContentIntent(activity);

        Notification notification = builder.build();

        Intent notificationIntent = new Intent(context, Reciever.class);
        notificationIntent.putExtra(NOTIFICATION_ID, notificationId);
        notificationIntent.putExtra(NOTIFICATION, notification);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, notificationId, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, date, pendingIntent); // Set the alarm to fire on the set date

        pendingNotifs.put(notificationId, pendingIntent);
    }

    public static boolean deleteNotification(Context context, int id) {
        PendingIntent pending = pendingNotifs.remove(id);
        if (pending != null) {
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            alarmManager.cancel(pending);
            return true;
        } else {
            return false;
        }
    }

    public static class Reciever extends BroadcastReceiver {
        public Reciever() {
            super();
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            Notification notification = intent.getParcelableExtra(NOTIFICATION);
            int notificationId = intent.getIntExtra(NOTIFICATION_ID, 0);
            notificationManager.notify(notificationId, notification);
        }

    }

}
