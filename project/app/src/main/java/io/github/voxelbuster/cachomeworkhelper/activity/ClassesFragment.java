package io.github.voxelbuster.cachomeworkhelper.activity;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Toast;

import java.util.ArrayList;

import io.github.voxelbuster.cachomeworkhelper.R;
import io.github.voxelbuster.cachomeworkhelper.util.ClassStruct;
import io.github.voxelbuster.cachomeworkhelper.util.DataManager;

public class ClassesFragment extends Fragment {
    private View fragmentView;

    private ListView classes_lv;
    private Button create_class_button;

    private ArrayAdapter<String> classesAdapter;

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentView = inflater.inflate(R.layout.classes_layout, container, false);
        classes_lv = (ListView) fragmentView.findViewById(R.id.classes_list);
        classes_lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try {
                    ClassStruct classStruct = new DataManager(ClassesFragment.this.getActivity()).getClasses()[position];
                    Intent intent = new Intent(ClassesFragment.this.getActivity().getBaseContext(), CreateClassActivity.class);
                    intent.putExtra("className", classStruct.getClassName());
                    intent.putExtra("teacher", classStruct.getTeacher());
                    intent.putExtra("time", classStruct.getTime());
                    intent.putExtra("code", classStruct.getClassCode());
                    intent.putExtra("editing", position);
                    ClassesFragment.this.getActivity().startActivity(intent);
                    classesAdapter.remove(classesAdapter.getItem(position));
                } catch (Exception e) {
                    Toast.makeText(ClassesFragment.this.getActivity().getBaseContext(), "Failed to parse classes.json. If the problem persists, clear your data and try again.", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        });

        classes_lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                PopupMenu popup = new PopupMenu(ClassesFragment.this.getActivity(), view);
                popup.getMenuInflater().inflate(R.menu.class_popup, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        if (item.getItemId() == R.id.del_class) {
                            try {
                                new DataManager(ClassesFragment.this.getActivity()).delClass(position);
                                ClassesFragment.this.onResume(); // Refreshes the list
                            } catch (Exception e) {
                                Toast.makeText(ClassesFragment.this.getActivity().getBaseContext(), "Failed to parse classes.json. If the problem persists, clear your data and try again.", Toast.LENGTH_SHORT).show();
                                e.printStackTrace();
                            }
                        } else if (item.getItemId() == R.id.edit_class) {
                            try {
                                DataManager dm = new DataManager(ClassesFragment.this.getActivity());
                                ClassStruct[] classes = dm.getClasses();
                                Intent intent = new Intent(ClassesFragment.this.getActivity().getBaseContext(), CreateClassActivity.class);
                                intent.putExtra("editing", position);
                                intent.putExtra("className", classes[position].getClassName());
                                intent.putExtra("teacher", classes[position].getTeacher());
                                intent.putExtra("time", classes[position].getTime());
                                intent.putExtra("code", classes[position].getClassCode());
                                ClassesFragment.this.getActivity().startActivity(intent);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        return true;
                    }
                });
                popup.show();
                return true;
            }
        });

        create_class_button = (Button) fragmentView.findViewById(R.id.add_class_button);
        create_class_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ClassesFragment.this.getActivity(), CreateClassActivity.class);
                ClassesFragment.this.startActivity(intent);
            }
        });
        return fragmentView;
    }

    @Override
    public void onResume() {
        super.onResume();
        ArrayList<String> classNames = new ArrayList<>();
        try {
            for (ClassStruct c : new DataManager(getActivity()).getClasses()) {
                classNames.add(c.getClassName());
            }
        } catch (Exception e) {
            Toast.makeText(ClassesFragment.this.getActivity().getBaseContext(), "Failed to parse classes.json. If the problem persists, clear your data and try again.", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
        classesAdapter = new ArrayAdapter<String>(fragmentView.getContext(), R.layout.list_view_layout, R.id.lv_text, classNames);
        classes_lv.setAdapter(classesAdapter);
        classesAdapter.notifyDataSetChanged();
    }
}