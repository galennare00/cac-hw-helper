package io.github.voxelbuster.cachomeworkhelper.util;

public class ClassStruct {
    private String teacher;
    private String name;
    private String time;
    private String code;

    public ClassStruct(String name, String teacher, String time, String code) {
        this.name = name;
        this.teacher = teacher;
        this.time = time;
        this.code = code;
    }

    public String getClassName() {
        return name;
    }

    public String getTeacher() {
        return teacher;
    }

    public String getTime() {
        return time;
    }

    public String getClassCode() {
        return code;
    }
}
