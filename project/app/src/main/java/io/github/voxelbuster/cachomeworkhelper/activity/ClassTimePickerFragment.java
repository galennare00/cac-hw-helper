package io.github.voxelbuster.cachomeworkhelper.activity;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import java.util.Calendar;

public class ClassTimePickerFragment extends DialogFragment {
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int minute = c.get(Calendar.MINUTE);
        int hour = c.get(Calendar.HOUR_OF_DAY);

        // Create a new instance of DatePickerDialog and return it
        return new TimePickerDialog(getActivity(), ((CreateClassActivity)getActivity()).getTimeSetListener(), hour, minute, false);
    }
}
