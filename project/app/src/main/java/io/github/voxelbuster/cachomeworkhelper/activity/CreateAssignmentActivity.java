package io.github.voxelbuster.cachomeworkhelper.activity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TimePicker;

import java.util.ArrayList;

import io.github.voxelbuster.cachomeworkhelper.R;
import io.github.voxelbuster.cachomeworkhelper.service.NotificationService;
import io.github.voxelbuster.cachomeworkhelper.util.APITools;
import io.github.voxelbuster.cachomeworkhelper.util.AssignmentStruct;
import io.github.voxelbuster.cachomeworkhelper.util.ClassStruct;
import io.github.voxelbuster.cachomeworkhelper.util.DataManager;
import io.github.voxelbuster.cachomeworkhelper.util.NotifStruct;

public class CreateAssignmentActivity extends AppCompatActivity {

    private EditText date_edit;
    private EditText time_edit;
    private Button cancel_button;
    private Button save_button;
    private Button new_notif;
    private ListView notifs_listview;
    private Spinner class_spinner;

    private ArrayList<NotifStruct> notifs = new ArrayList<>();
    private ArrayList<String> notifStrs = new ArrayList<String>();
    private ArrayAdapter<String> notifs_adapter;

    private String timeScale;
    private int time;
    private boolean after;
    private boolean persist;

    private EditText name_edit, link_edit;
    private Switch nag_sw;

    private int editing;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_assignment);

        date_edit = (EditText) findViewById(R.id.date_edit);
        date_edit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    DatePickerFragment dpf = new DatePickerFragment();
                    dpf.show(getSupportFragmentManager(), "datePicker");
                }
            }
        });
        time_edit = (EditText) findViewById(R.id.time_edit);
        time_edit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    TimePickerFragment tpf = new TimePickerFragment();
                    tpf.show(getSupportFragmentManager(), "timePicker");
                }
            }
        });
        class_spinner = (Spinner) findViewById(R.id.class_spinner);
        ClassStruct[] pulledClasses = new ClassStruct[0];
        try {
            pulledClasses = new DataManager(this).getClasses();
        } catch (Exception e) {
            e.printStackTrace();
        }
        String[] classNames = new String[pulledClasses.length];
        for (int i=0;i<pulledClasses.length;i++) {
            classNames[i] = pulledClasses[i].getClassName();
        }
        ArrayAdapter<String> classesAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, classNames);
        classesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        class_spinner.setAdapter(classesAdapter);
        classesAdapter.notifyDataSetChanged();
        class_spinner.setSelection(0);

        cancel_button = (Button) findViewById(R.id.cancel_button);
        cancel_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        notifs_listview = (ListView) findViewById(R.id.notifs_listview);

        name_edit = (EditText) findViewById(R.id.name_edit);
        link_edit = (EditText) findViewById(R.id.link_edit);
        nag_sw = (Switch) findViewById(R.id.nag_switch);

        editing = getIntent().getIntExtra("editing", -1);
        String dateArg = getIntent().getStringExtra("date");
        if (dateArg != null) {
            date_edit.setText(dateArg); // Allow calendar fragment to pass a date
        }
        if (editing > -1) {  // populate form
            try {
                AssignmentStruct struct = new DataManager(this).getAssignments()[editing];
                name_edit.setText(struct.getName());
                class_spinner.setSelection(classesAdapter.getPosition(struct.getClassName()));
                time_edit.setText(struct.getTime());
                date_edit.setText(struct.getDate());
                link_edit.setText(struct.getURI());
                nag_sw.setChecked(struct.isNag());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        new_notif = (Button) findViewById(R.id.add_notif_button);
        new_notif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final View rootView = CreateAssignmentActivity.this.getLayoutInflater().inflate(R.layout.fragment_notif_dialog, null);
                final Switch before_after_sw = (Switch) rootView.findViewById(R.id.before_after_switch);
                before_after_sw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            before_after_sw.setText("After");
                        } else {
                            before_after_sw.setText("Before");
                        }
                    }
                });
                final AlertDialog.Builder builder = new AlertDialog.Builder(CreateAssignmentActivity.this);
                builder.setMessage("Add new notification")
                        .setView(rootView)
                        .setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                String str;
                                NotifStruct newNotif = APITools.createNotif(time, timeScale, persist, after);
                                if (newNotif != null) {
                                    notifs.add(newNotif);
                                    if (newNotif.isLateNotif()) {
                                        str = "" + time + " " + timeScale +
                                                " after";
                                    } else {
                                        str = "" + time + " " + timeScale +
                                                " before";
                                    }
                                    notifStrs.add(str);
                                    notifs_adapter = new ArrayAdapter<String>(CreateAssignmentActivity.this, R.layout.list_view_layout, R.id.lv_text, notifStrs);
                                    notifs_listview.setAdapter(notifs_adapter);
                                    notifs_adapter.notifyDataSetChanged();
                                }
                            }
                        })
                        .setPositiveButton("Add", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                time = Integer.parseInt(((EditText) rootView.findViewById(R.id.quantity_time_edit)).getText().toString());
                                timeScale = ((Spinner) rootView.findViewById(R.id.time_scale_spinner)).getSelectedItem().toString();
                                after = ((Switch) rootView.findViewById(R.id.before_after_switch)).isChecked();
                                persist = ((Switch) rootView.findViewById(R.id.persist_switch)).isChecked();

                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {}
                        });
                builder.create().show();
            }
        });

        save_button = (Button) findViewById(R.id.save_button);
        save_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String dateStr = ((EditText) findViewById(R.id.date_edit)).getText().toString();
                    String timeStr = ((EditText) findViewById(R.id.time_edit)).getText().toString();
                    String nameStr = ((EditText) findViewById(R.id.name_edit)).getText().toString();
                    new DataManager(CreateAssignmentActivity.this).putAssignment(nameStr,
                            ((Spinner) findViewById(R.id.class_spinner)).getSelectedItem().toString(),
                            dateStr,
                            timeStr,
                            ((EditText) findViewById(R.id.link_edit)).getText().toString(),
                            notifs.toArray(new NotifStruct[notifs.size()]),
                            ((Switch) findViewById(R.id.nag_switch)).isChecked(), false);
                    if (editing >= -1) {
                        new DataManager(CreateAssignmentActivity.this).delAssign(CreateAssignmentActivity.this.getApplicationContext(), editing);
                        editing = -1;
                    }
                    for (NotifStruct n : notifs) {
                        Log.d("TimeCheckLater", "" + (APITools.convertDateString(dateStr + " " + timeStr) + APITools.convertToMs(n.getQuantity(), n.getTimeScale()) > System.currentTimeMillis()));
                        Log.d("TimeCheckNow", "" + (APITools.convertDateString(dateStr + " " + timeStr) + APITools.convertToMs(n.getQuantity(), n.getTimeScale()) == System.currentTimeMillis()));
                        if (n.isStickyNotif()) {
                            NotificationService.schedulePersistentNotification(CreateAssignmentActivity.this.getApplicationContext(), APITools.convertDateString(dateStr + " " + timeStr) + APITools.convertToMs(n.getQuantity(), n.getTimeScale()), n.getNumericId(), "Homework Helper", nameStr);
                        } else {
                            NotificationService.scheduleNotification(CreateAssignmentActivity.this.getApplicationContext(), APITools.convertDateString(dateStr + " " + timeStr) + APITools.convertToMs(n.getQuantity(), n.getTimeScale()), n.getNumericId(), "Homework Helper", nameStr);
                        }
                    }
                    if (((Switch) findViewById(R.id.nag_switch)).isChecked()) {
                        NotificationService.schedulePersistentNotification(CreateAssignmentActivity.this.getApplicationContext(), APITools.convertDateString(dateStr + " " + timeStr), 32670, "Overdue Assignment", nameStr);
                    }
                    finish();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public DatePickerDialog.OnDateSetListener getDateSetListener() {
        return new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int day) {
                date_edit.setText((month+1) + "/" + day + "/" + year);
            }
        };

    }

    public TimePickerDialog.OnTimeSetListener getTimeSetListener() {
        return new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                time_edit.setText(String.format("%02d",hourOfDay) + ":" + String.format("%02d",minute));
            }
        };

    }
}