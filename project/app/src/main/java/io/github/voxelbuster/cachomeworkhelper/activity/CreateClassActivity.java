package io.github.voxelbuster.cachomeworkhelper.activity;

import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import io.github.voxelbuster.cachomeworkhelper.R;
import io.github.voxelbuster.cachomeworkhelper.util.DataManager;

public class CreateClassActivity extends AppCompatActivity {

    private EditText class_time;
    private Button cancel_button;
    private Button save_button;

    private EditText class_name_edit, teacher_edit, time_class_edit, class_code_edit;

    FirebaseDatabase database = FirebaseDatabase.getInstance();

    private int editing = -1;
    private boolean can_finish = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_class);
        class_name_edit = (EditText) findViewById(R.id.class_name_edit);
        teacher_edit = (EditText) findViewById(R.id.teacher_edit);
        time_class_edit = (EditText) findViewById(R.id.time_class_edit);
        class_code_edit = (EditText) findViewById(R.id.class_code_edit);

        if (getIntent().getStringExtra("className") != null) {
            class_name_edit.setText(getIntent().getStringExtra("className"));
        } if (getIntent().getStringExtra("teacher") != null) {
            teacher_edit.setText(getIntent().getStringExtra("teacher"));
        } if (getIntent().getStringExtra("code") != null) {
            class_code_edit.setText(getIntent().getStringExtra("code"));
        } if (getIntent().getStringExtra("time") != null) {
            time_class_edit.setText(getIntent().getStringExtra("time"));
        } if (getIntent().getIntExtra("editing", -1) > -1) {
            editing = getIntent().getIntExtra("editing", -1);
        }

        class_time = (EditText) findViewById(R.id.time_class_edit);
        class_time.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    ClassTimePickerFragment tpf = new ClassTimePickerFragment();
                    tpf.show(getSupportFragmentManager(), "timePicker");
                }
            }
        });
        cancel_button = (Button) findViewById(R.id.cancel_class_button);
        cancel_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        save_button = (Button) findViewById(R.id.save_class_button);
        save_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                can_finish = true;
                try {
                    if (editing > -1) {
                        new DataManager(CreateClassActivity.this).delClass(editing);
                        editing = -1;
                    }
                    if (class_code_edit.getText().toString() != "") {
                        DatabaseReference ref = database.getReference();
                        ref.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                DataSnapshot classSnapshot = dataSnapshot.child("classes").child(class_code_edit.getText().toString());
                                if (classSnapshot.exists()) {
                                    class_name_edit.setText(classSnapshot.child("name").getValue().toString());
                                    teacher_edit.setText(classSnapshot.child("teacher").getValue().toString());
                                    time_class_edit.setText(classSnapshot.child("time").getValue().toString());
                                } else {
                                    Toast.makeText(CreateClassActivity.this.getApplicationContext(), "Invalid class code", Toast.LENGTH_SHORT).show();
                                    class_code_edit.setText("");
                                    can_finish = false;
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                Toast.makeText(CreateClassActivity.this.getApplicationContext(), "Something went wrong. Check your internet connection and try again.", Toast.LENGTH_SHORT).show();
                                can_finish = false;
                            }
                        });
                    }
                    if (can_finish) {
                        new DataManager(CreateClassActivity.this).putClass(class_name_edit.getText().toString(),
                                teacher_edit.getText().toString(),
                                time_class_edit.getText().toString(),
                                class_code_edit.getText().toString());
                        finish();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public TimePickerDialog.OnTimeSetListener getTimeSetListener() {
        return new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                class_time.setText(String.format("%02d",hourOfDay) + ":" + String.format("%02d",minute));
            }
        };
    }
}
