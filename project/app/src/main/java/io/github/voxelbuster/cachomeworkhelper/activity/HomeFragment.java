package io.github.voxelbuster.cachomeworkhelper.activity;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

import io.github.voxelbuster.cachomeworkhelper.R;
import io.github.voxelbuster.cachomeworkhelper.util.AssignmentStruct;
import io.github.voxelbuster.cachomeworkhelper.util.DataManager;
import io.github.voxelbuster.cachomeworkhelper.util.NotifStruct;

public class HomeFragment extends Fragment {
    private View fragmentView;
    
    private ListView assign_lv;

    private ArrayAdapter<String> assignAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentView = inflater.inflate(R.layout.home_layout, container, false);
        assign_lv = (ListView) fragmentView.findViewById(R.id.list_assignments_view);
        assign_lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try {
                    AssignmentStruct assignStruct = new DataManager(HomeFragment.this.getActivity()).getAssignments()[position];
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    String url = assignStruct.getURI();
                    if (!url.matches("(?:(?:https?|ftp|file):\\/\\/|www\\.|ftp\\.)(?:\\([-A-Z0-9+&@#\\/%=~_|$?!:,.]*\\)|[-A-Z0-9+&@#\\/%=~_|$?!:,.])*(?:\\([-A-Z0-9+&@#\\/%=~_|$?!:,.]*\\)|[A-Z0-9+&@#\\/%=~_|$])")) { // allow users to omit protocol in URLs
                        url = "http://" + url;
                    }
                    intent.setData(Uri.parse(url));
                    getActivity().startActivity(intent);
                } catch (Exception e) {
                    Toast.makeText(HomeFragment.this.getActivity().getBaseContext(), "Failed to parse assignments.json. If the problem persists, clear your data and try again.", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        });

        assign_lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                PopupMenu popup = new PopupMenu(HomeFragment.this.getActivity(), view);
                popup.getMenuInflater().inflate(R.menu.assign_popup, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        if (item.getItemId() == R.id.del_assign) {
                            try {
                                new DataManager(HomeFragment.this.getActivity()).delAssign(HomeFragment.this.getActivity().getBaseContext(), position);
                                HomeFragment.this.onResume(); // Refreshes the list
                            } catch (Exception e) {
                                Toast.makeText(HomeFragment.this.getActivity().getBaseContext(), "Failed to parse assignments.json. If the problem persists, clear your data and try again.", Toast.LENGTH_SHORT).show();
                                e.printStackTrace();
                            }
                        } else if (item.getItemId() == R.id.edit_assign) {
                            try {
                                AssignmentStruct assignStruct = new DataManager(HomeFragment.this.getActivity()).getAssignments()[position];
                                Intent intent = new Intent(HomeFragment.this.getActivity().getBaseContext(), CreateAssignmentActivity.class);
                                intent.putExtra("name", assignStruct.getName());
                                intent.putExtra("className", assignStruct.getClassName());
                                intent.putExtra("time", assignStruct.getTime());
                                intent.putExtra("date", assignStruct.getDate());
                                intent.putExtra("uri", assignStruct.getURI());
                                intent.putExtra("nag", assignStruct.isNag());
                                ArrayList<NotifStruct> notifArray = new ArrayList<>(Arrays.asList(assignStruct.getNotifs()));
                                ArrayList<Integer> notifIdArray = new ArrayList<>();
                                for (NotifStruct n : notifArray) {
                                    notifIdArray.add(n.getNumericId());
                                }
                                intent.putIntegerArrayListExtra("notifs", notifIdArray);
                                intent.putExtra("editing", position);
                                HomeFragment.this.getActivity().startActivity(intent);
                                assignAdapter.remove(assignAdapter.getItem(position));
                            } catch (Exception e) {
                                Toast.makeText(HomeFragment.this.getActivity().getBaseContext(), "Failed to parse assignments.json. If the problem persists, clear your data and try again.", Toast.LENGTH_SHORT).show();
                                e.printStackTrace();
                            }
                        } else if (item.getItemId() == R.id.comp_assign) {
                            try {
                                DataManager dm = new DataManager(HomeFragment.this.getActivity());
                                AssignmentStruct assignStruct = dm.getAssignments()[position];
                                if (assignStruct.isCompleted()) return false;
                                AssignmentStruct completed = new AssignmentStruct("✔ " + assignStruct.getName(),
                                        assignStruct.getClassName(),
                                        assignStruct.getDate(),
                                        assignStruct.getTime(),
                                        assignStruct.getURI(),
                                        assignStruct.getNotifs(),
                                        assignStruct.isNag(),
                                        true);
                                assignAdapter.remove(assignAdapter.getItem(position));
                                dm.putAssignment(completed.getName(), completed.getClassName(), completed.getDate(), completed.getTime(),
                                        completed.getURI(), completed.getNotifs(), completed.isNag(), completed.isCompleted());
                                assignAdapter.add(completed.getName());
                                new DataManager(HomeFragment.this.getActivity()).delAssign(HomeFragment.this.getActivity().getBaseContext(), position);
                                HomeFragment.this.onResume();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        return true;
                    }
                });
                popup.show();
                return true;
            }
        });
        
        return fragmentView;
    }

    @Override
    public void onResume() {
        super.onResume();
        ArrayList<String> names = new ArrayList<>();
        try {
            for (AssignmentStruct c : new DataManager(getActivity()).getAssignments()) {
                if (Calendar.getInstance().getTime().before(new SimpleDateFormat("MM/dd/yyyy").parse(c.getDate()))) { // Only show assignments that aren't overdue
                    names.add(c.getName());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        assignAdapter = new ArrayAdapter<String>(fragmentView.getContext(), R.layout.list_view_layout, R.id.lv_text, names);
        assign_lv.setAdapter(assignAdapter);
        assignAdapter.notifyDataSetChanged();
    }

}