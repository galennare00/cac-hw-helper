package io.github.voxelbuster.cachomeworkhelper.util;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import io.github.voxelbuster.cachomeworkhelper.service.NotificationService;

public class DataManager {
    private final String classesJson;
    private final String assignmentsJson;

    private final BufferedReader classesReader;
    private final BufferedReader assignmentsReader;
    
    private BufferedWriter classesWriter;
    private BufferedWriter assignmentsWriter;

    public DataManager(Activity parent) throws IOException, PackageManager.NameNotFoundException {
        String s = parent.getFilesDir().getAbsolutePath();

        classesJson = s + "/userdata/classes.json";
        assignmentsJson = s + "/userdata/assignments.json";

        Log.d("HH", "ClassesJson path: " + classesJson);

        if (!new File(classesJson).exists()) {
            new File(s + "/userdata").mkdirs();
            new File(classesJson).createNewFile();
            BufferedWriter writer = new BufferedWriter(new FileWriter(classesJson));
            writer.write("{\"classes\":[]}");
            writer.close();
        }

        if (!new File(assignmentsJson).exists()) {
            new File(s + "/userdata").mkdirs();
            new File(assignmentsJson).createNewFile();
            BufferedWriter writer = new BufferedWriter(new FileWriter(assignmentsJson));
            writer.write("{\"assignments\":[]}");
            writer.close();
        }

        this.classesReader = new BufferedReader(new FileReader(classesJson));
        this.assignmentsReader = new BufferedReader(new FileReader(assignmentsJson));
    }

    /*
    demo classes.json

    {
        "classes":[
            {
                "name":"Class 1",
                "teacher":"Mr. Smith",
                "time":"13:00",
                "code":"abc123" // 6 characters (a-z,A-Z,0-9)
            }
        ]
    }
     */

    public ClassStruct[] getClasses() throws JSONException, IOException {
        classesReader.mark(2);

        StringBuilder result = new StringBuilder();
        String line;
        while ((line = classesReader.readLine()) != null) {
            result.append(line);
        }
        JSONObject root = new JSONObject(result.toString());
        JSONArray classes = root.getJSONArray("classes");
        ClassStruct[] classStructs = new ClassStruct[classes.length()];
        for (int i=0;i<classes.length();i++) {
            JSONObject singleClass = classes.getJSONObject(i);
            classStructs[i] = new ClassStruct(singleClass.getString("name"), singleClass.getString("teacher"), singleClass.getString("time"), singleClass.getString("code"));
        }
        classesReader.reset();
        return classStructs;
    }
    
    public void putClass(String name, String teacher, String time, String code) throws JSONException, IOException {
        JSONObject classObj = new JSONObject();
        classObj.put("name", name);
        classObj.put("teacher", teacher);
        classObj.put("time", time);
        classObj.put("code", code);

        classesReader.mark(2);

        StringBuilder result = new StringBuilder();
        String line;
        while ((line = classesReader.readLine()) != null) {
            result.append(line);
        }
        JSONObject root = new JSONObject(result.toString());
        JSONArray classes = root.getJSONArray("classes");

        classes.put(classObj);

        JSONObject newRoot = new JSONObject();
        newRoot.put("classes", classes);

        classesReader.reset();
        this.classesWriter = new BufferedWriter(new FileWriter(classesJson));
        classesWriter.write(newRoot.toString());
        classesWriter.flush();
    }

    /*
        demo assignments.json

        {
            "assignments":[
                {
                    "name":"homework 1",
                    "class":"Math",
                    "date":"01/01/1970",
                    "time":"12:00",
                    "uri":"file:///storage/emulated/0/homework.pdf",
                    "notifs":[
                        "time":5,
                        "scale":"minutes",
                        "sticky":false,
                        "after":false
                    ],
                    "completed":false,
                    "nag":false
                }
            ]
        }
     */
    public AssignmentStruct[] getAssignments() throws JSONException, IOException {
        assignmentsReader.mark(2);

        StringBuilder result = new StringBuilder();
        String line;
        while ((line = assignmentsReader.readLine()) != null) {
            result.append(line);
        }
        JSONObject root = new JSONObject(result.toString());
        JSONArray assignments = root.getJSONArray("assignments");
        AssignmentStruct[] assignStructs = new AssignmentStruct[assignments.length()];
        for (int i=0;i<assignments.length();i++) {
            JSONObject singleAssign = assignments.getJSONObject(i);
            NotifStruct[] notifs = new NotifStruct[singleAssign.getJSONArray("notifs").length()];
            for (int j=0;j<notifs.length;j++) {
                NotifStruct.TimeScale scale = null;
                String scaleStr = ((JSONObject)singleAssign.getJSONArray("notifs").get(j)).getString("scale");
                switch (scaleStr) {
                    case "seconds":
                        scale = NotifStruct.TimeScale.SECONDS;
                        break;
                    case "minutes":
                        scale = NotifStruct.TimeScale.MINUTES;
                        break;
                    case "hours":
                        scale = NotifStruct.TimeScale.HOURS;
                        break;
                    case "days":
                        scale = NotifStruct.TimeScale.DAYS;
                        break;
                    case "weeks":
                        scale = NotifStruct.TimeScale.WEEKS;
                        break;
                    case "months":
                        scale = NotifStruct.TimeScale.MONTHS;
                        break;
                    default:
                        scale = NotifStruct.TimeScale.HOURS;
                        break;
                }
                notifs[j] = new NotifStruct(((JSONObject)singleAssign.getJSONArray("notifs").get(j)).getInt("time"), scale, ((JSONObject)singleAssign.getJSONArray("notifs").get(j)).getBoolean("sticky"), ((JSONObject)singleAssign.getJSONArray("notifs").get(j)).getBoolean("after"), ((JSONObject)singleAssign.getJSONArray("notifs").get(j)).getInt("id"));
            }
            assignStructs[i] = new AssignmentStruct(singleAssign.getString("name"), singleAssign.getString("class"),
                    singleAssign.getString("date"), singleAssign.getString("time"),
                    singleAssign.getString("uri"), notifs, singleAssign.getBoolean("nag"), singleAssign.getBoolean("completed"));
        }
        assignmentsReader.reset();
        return assignStructs;
    }

    public void putAssignment(String name, String className, String date, String time, String uri, NotifStruct[] notifs, boolean nag, boolean complete) throws JSONException, IOException {
        JSONObject assignObj = new JSONObject();
        assignObj.put("name", name);
        assignObj.put("class", className);
        assignObj.put("date", date);
        assignObj.put("time", time);
        assignObj.put("uri", uri);

        JSONArray notifsJson = new JSONArray();

        for (NotifStruct n : notifs) {
            JSONObject notifObj = new JSONObject();
            notifObj.put("time", n.getQuantity());
            switch (n.getTimeScale()) {
                case SECONDS:
                    notifObj.put("scale","seconds");
                    break;
                case MINUTES:
                    notifObj.put("scale","minutes");
                    break;
                case HOURS:
                    notifObj.put("scale","hours");
                    break;
                case DAYS:
                    notifObj.put("scale","days");
                    break;
                case WEEKS:
                    notifObj.put("scale","weeks");
                    break;
                case MONTHS:
                    notifObj.put("scale","months");
                    break;
            }
            notifObj.put("sticky", n.isStickyNotif());
            notifObj.put("after", n.isLateNotif());
            notifObj.put("id", n.getNumericId());

            notifsJson.put(notifObj);
        }

        assignObj.put("notifs", notifsJson);
        assignObj.put("nag", nag);
        assignObj.put("completed", complete);

        assignmentsReader.mark(2);

        StringBuilder result = new StringBuilder();
        String line;
        while ((line = assignmentsReader.readLine()) != null) {
            result.append(line);
        }
        JSONObject root = new JSONObject(result.toString());
        JSONArray assignments = root.getJSONArray("assignments");

        assignments.put(assignObj);

        JSONObject newRoot = new JSONObject();
        newRoot.put("assignments", assignments);

        assignmentsReader.reset();
        this.assignmentsWriter = new BufferedWriter(new FileWriter(assignmentsJson));
        assignmentsWriter.write(newRoot.toString());
        assignmentsWriter.flush();
    }

    public boolean delClass(int index) throws IOException, JSONException {
        classesReader.mark(2);

        StringBuilder result = new StringBuilder();
        String line;
        while ((line = classesReader.readLine()) != null) {
            result.append(line);
        }
        JSONObject root = new JSONObject(result.toString());
        JSONArray classes = root.getJSONArray("classes");
        JSONArray newClasses = new JSONArray();
        JSONObject newRoot = new JSONObject();
        if (index >= classes.length()) {return false;}
        for (int i=0;i<classes.length();i++) {
            if (i == index) {
                continue;
            } else {
                newClasses.put(classes.getJSONObject(i));
            }
        }
        newRoot.put("classes", newClasses);

        classesReader.reset();
        this.classesWriter = new BufferedWriter(new FileWriter(classesJson));
        classesWriter.write(newRoot.toString());
        classesWriter.flush();
        return true;
    }

    public boolean delAssign(Context context, int index) throws IOException, JSONException {
        assignmentsReader.mark(2);

        StringBuilder result = new StringBuilder();
        String line;
        while ((line = assignmentsReader.readLine()) != null) {
            result.append(line);
        }
        JSONObject root = new JSONObject(result.toString());
        JSONArray assignments = root.getJSONArray("assignments");
        JSONArray newAssignments = new JSONArray();
        JSONObject newRoot = new JSONObject();
        if (index >= assignments.length()) {return false;}
        for (int i=0;i<assignments.length();i++) {
            if (i == index) {
                JSONObject singleAssign = assignments.getJSONObject(i);
                NotifStruct[] notifs = new NotifStruct[singleAssign.getJSONArray("notifs").length()];
                for (int j=0;j<notifs.length;j++) {
                    NotifStruct.TimeScale scale = null;
                    String scaleStr = ((JSONObject)singleAssign.getJSONArray("notifs").get(j)).getString("scale");
                    switch (scaleStr) {
                        case "seconds":
                            scale = NotifStruct.TimeScale.SECONDS;
                            break;
                        case "minutes":
                            scale = NotifStruct.TimeScale.MINUTES;
                            break;
                        case "hours":
                            scale = NotifStruct.TimeScale.HOURS;
                            break;
                        case "days":
                            scale = NotifStruct.TimeScale.DAYS;
                            break;
                        case "weeks":
                            scale = NotifStruct.TimeScale.WEEKS;
                            break;
                        case "months":
                            scale = NotifStruct.TimeScale.MONTHS;
                            break;
                        default:
                            scale = NotifStruct.TimeScale.HOURS;
                            break;
                    }
                    notifs[j] = new NotifStruct(((JSONObject)singleAssign.getJSONArray("notifs").get(j)).getInt("time"), scale, ((JSONObject)singleAssign.getJSONArray("notifs").get(j)).getBoolean("sticky"), ((JSONObject)singleAssign.getJSONArray("notifs").get(j)).getBoolean("after"), ((JSONObject)singleAssign.getJSONArray("notifs").get(j)).getInt("id"));
                }
                AssignmentStruct assignStruct = new AssignmentStruct(singleAssign.getString("name"), singleAssign.getString("class"),
                        singleAssign.getString("date"), singleAssign.getString("time"),
                        singleAssign.getString("uri"), notifs, singleAssign.getBoolean("nag"), false);
                for (NotifStruct n : notifs) {
                    NotificationService.deleteNotification(context, n.getNumericId());
                }
                NotificationService.deleteNotification(context, 32670);
                NotificationManager notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.cancel(32670);
                continue;
            } else {
                newAssignments.put(assignments.getJSONObject(i));
            }
        }
        newRoot.put("assignments", newAssignments);

        assignmentsReader.reset();
        this.assignmentsWriter = new BufferedWriter(new FileWriter(assignmentsJson));
        assignmentsWriter.write(newRoot.toString());
        assignmentsWriter.flush();
        return true;
    }

    public static void deleteDirectory(File file) {
        if( file.exists() ) {
            if (file.isDirectory()) {
                File[] files = file.listFiles();
                for(int i=0; i<files.length; i++) {
                    if(files[i].isDirectory()) {
                        deleteDirectory(files[i]);
                    }
                    else {
                        files[i].delete();
                    }
                }
            }
            file.delete();
        }
    }
}
