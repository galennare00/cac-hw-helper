package io.github.voxelbuster.cachomeworkhelper.activity;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import io.github.voxelbuster.cachomeworkhelper.R;
import io.github.voxelbuster.cachomeworkhelper.util.AssignmentStruct;
import io.github.voxelbuster.cachomeworkhelper.util.DataManager;
import io.github.voxelbuster.cachomeworkhelper.util.NotifStruct;

public class AssignmentsFragment extends Fragment {
    private View fragmentView;

    private ListView assign_lv;
    private EditText search_bar;

    private Spinner sort_spinner;

    private ArrayAdapter<String> assignAdapter;

    private ArrayList<String> items = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentView = inflater.inflate(R.layout.assignments_layout, container, false);
        FloatingActionButton fab = (FloatingActionButton) fragmentView.findViewById(R.id.assignment_add_button);
        assign_lv = (ListView) fragmentView.findViewById(R.id.assignments_listview);
        assign_lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try {
                    AssignmentStruct assignStruct = new DataManager(AssignmentsFragment.this.getActivity()).getAssignments()[position];
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    String url = assignStruct.getURI();
                    if (!url.matches("(?:(?:https?|ftp|file):\\/\\/|www\\.|ftp\\.)(?:\\([-A-Z0-9+&@#\\/%=~_|$?!:,.]*\\)|[-A-Z0-9+&@#\\/%=~_|$?!:,.])*(?:\\([-A-Z0-9+&@#\\/%=~_|$?!:,.]*\\)|[A-Z0-9+&@#\\/%=~_|$])")) { // allow users to omit protocol in URLs
                        url = "http://" + url;
                    }
                    intent.setData(Uri.parse(url));
                    getActivity().startActivity(intent);
                } catch (Exception e) {
                    Toast.makeText(AssignmentsFragment.this.getActivity().getBaseContext(), "Failed to parse assignments.json. If the problem persists, clear your data and try again.", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        });

        sort_spinner = (Spinner) fragmentView.findViewById(R.id.sort_spinner); // fix this later sorting is hard
        /*sort_spinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        Collections.sort(items);
                        assignAdapter.clear();
                        for (String item: items) {
                            assignAdapter.add(item);
                        }
                        break;
                    case 1:

                }
                assignAdapter = new ArrayAdapter<>(fragmentView.getContext(), R.layout.list_view_layout, R.id.lv_text, items);
                assign_lv.setAdapter(assignAdapter);
                assignAdapter.notifyDataSetChanged();
            }
        });*/

        search_bar = (EditText) fragmentView.findViewById(R.id.search_edit);
        search_bar.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                search_bar.setText("");
                onResume();
                return true;
            }
        });

        ImageButton search_button = (ImageButton) fragmentView.findViewById(R.id.search_button);
        search_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onResume();
                ArrayList<String> itemsToRemove = new ArrayList<>();
                for (String item : items) {
                    if (!item.toUpperCase().contains(search_bar.getText().toString().toUpperCase())) {
                        itemsToRemove.add(item);
                    }
                }
                for (String i : itemsToRemove){
                    assignAdapter.remove(i);
                }
                assignAdapter = new ArrayAdapter<>(fragmentView.getContext(), R.layout.list_view_layout, R.id.lv_text, items);
                assign_lv.setAdapter(assignAdapter);
                assignAdapter.notifyDataSetChanged();
            }
        });

        assign_lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                PopupMenu popup = new PopupMenu(AssignmentsFragment.this.getActivity(), view);
                popup.getMenuInflater().inflate(R.menu.assign_popup, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        if (item.getItemId() == R.id.del_assign) {
                            try {
                                new DataManager(AssignmentsFragment.this.getActivity()).delAssign(AssignmentsFragment.this.getActivity().getBaseContext(), position);
                                AssignmentsFragment.this.onResume(); // Refreshes the list
                            } catch (Exception e) {
                                Toast.makeText(AssignmentsFragment.this.getActivity().getBaseContext(), "Failed to parse assignments.json. If the problem persists, clear your data and try again.", Toast.LENGTH_SHORT).show();
                                e.printStackTrace();
                            }
                        } else if (item.getItemId() == R.id.edit_assign) {
                            try {
                                AssignmentStruct assignStruct = new DataManager(AssignmentsFragment.this.getActivity()).getAssignments()[position];
                                Intent intent = new Intent(AssignmentsFragment.this.getActivity().getBaseContext(), CreateAssignmentActivity.class);
                                intent.putExtra("name", assignStruct.getName());
                                intent.putExtra("className", assignStruct.getClassName());
                                intent.putExtra("time", assignStruct.getTime());
                                intent.putExtra("date", assignStruct.getDate());
                                intent.putExtra("uri", assignStruct.getURI());
                                intent.putExtra("nag", assignStruct.isNag());
                                ArrayList<NotifStruct> notifArray = new ArrayList<>(Arrays.asList(assignStruct.getNotifs()));
                                ArrayList<Integer> notifIdArray = new ArrayList<>();
                                for (NotifStruct n : notifArray) {
                                    notifIdArray.add(n.getNumericId());
                                }
                                intent.putIntegerArrayListExtra("notifs", notifIdArray);
                                intent.putExtra("editing", position);
                                AssignmentsFragment.this.getActivity().startActivity(intent);
                                assignAdapter.remove(assignAdapter.getItem(position));
                            } catch (Exception e) {
                                Toast.makeText(AssignmentsFragment.this.getActivity().getBaseContext(), "Failed to parse assignments.json. If the problem persists, clear your data and try again.", Toast.LENGTH_SHORT).show();
                                e.printStackTrace();
                            }
                        } else if (item.getItemId() == R.id.comp_assign) {
                            try {
                                DataManager dm = new DataManager(AssignmentsFragment.this.getActivity());
                                AssignmentStruct assignStruct = dm.getAssignments()[position];
                                if (assignStruct.isCompleted()) return false;
                                AssignmentStruct completed = new AssignmentStruct("✔ " + assignStruct.getName(),
                                        assignStruct.getClassName(),
                                        assignStruct.getDate(),
                                        assignStruct.getTime(),
                                        assignStruct.getURI(),
                                        assignStruct.getNotifs(),
                                        assignStruct.isNag(),
                                        true);
                                assignAdapter.remove(assignAdapter.getItem(position));
                                dm.putAssignment(completed.getName(), completed.getClassName(), completed.getDate(), completed.getTime(),
                                        completed.getURI(), completed.getNotifs(), completed.isNag(), completed.isCompleted());
                                assignAdapter.add(completed.getName());
                                new DataManager(AssignmentsFragment.this.getActivity()).delAssign(AssignmentsFragment.this.getActivity().getBaseContext(), position);
                                AssignmentsFragment.this.onResume();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        return true;
                    }
                });
                popup.show();
                return true;
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AssignmentsFragment.this.getActivity(), CreateAssignmentActivity.class);
                AssignmentsFragment.this.startActivity(intent);
            }
        });
        return fragmentView;
    }

    @Override
    public void onResume() { // Refresh adapter and Listview
        super.onResume();
        try {
            items.clear();
            for (AssignmentStruct c : new DataManager(getActivity()).getAssignments()) {
                items.add(c.getName());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (sort_spinner.getSelectedItem().toString().equals("Alphabetical")) Collections.sort(items);
        assignAdapter = new ArrayAdapter<>(fragmentView.getContext(), R.layout.list_view_layout, R.id.lv_text, items);
        assign_lv.setAdapter(assignAdapter);
        assignAdapter.notifyDataSetChanged();
    }
}