package io.github.voxelbuster.cachomeworkhelper.util;

public class AssignmentStruct {
    private final String name;
    private final String className;
    private final String date;
    private final String time;
    private final String uri;
    private final NotifStruct[] notifs;
    private final boolean nag, completed;

    public AssignmentStruct(String name, String className, String date, String time, String uri, NotifStruct[] notifs, boolean nag, boolean completed) {
        this.name = name;
        this.className = className;
        this.date = date;
        this.time = time;
        this.uri = uri;
        this.notifs = notifs;
        this.nag = nag;
        this.completed = completed;
    }

    public String getName() {
        return name;
    }

    public String getClassName() {
        return className;
    }

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

    public String getURI() {
        return uri;
    }

    public NotifStruct[] getNotifs() {
        return notifs;
    }

    public boolean isNag() {
        return nag;
    }

    public boolean isCompleted() {
        return completed;
    }
}
