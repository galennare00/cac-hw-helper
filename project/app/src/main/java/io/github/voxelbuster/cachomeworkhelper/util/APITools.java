package io.github.voxelbuster.cachomeworkhelper.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.Random;

public class APITools {
    public static long convertDateString(String date) throws ParseException {
        return new SimpleDateFormat("MM/dd/yyyy hh:mm").parse(date).getTime();
    }

    public static long convertToMs(Map<String, Integer> params) {
        long milliseconds = 0;
        if (params.get("sec") != null) {
            milliseconds += params.get("sec") * 1000L;
        } if (params.get("min") != null) {
            milliseconds += params.get("min") * 60000L;
        } if (params.get("hrs") != null) {
            milliseconds += params.get("hrs") * 3600000L;
        } if (params.get("day") != null) {
            milliseconds += params.get("day") * 86400000L;
        } if (params.get("wks") != null) {
            milliseconds += params.get("wks") * 604800000L;
        } if (params.get("mos") != null) {
            milliseconds += params.get("mos") * 2629743833L;
        }

        return milliseconds;
    }

    public static long convertToMs(long time, NotifStruct.TimeScale scale) {
        switch (scale) {
            case SECONDS:
                return time * 1000L;
            case MINUTES:
                return time * 60000L;
            case HOURS:
                return time * 3600000L;
            case DAYS:
                return time * 86400000L;
            case WEEKS:
                return time * 604800000L;
            case MONTHS:
                return time * 2629743833L;
            default:
                return 0;
        }
    }

    public static NotifStruct createNotif(int time, String timeScale, boolean sticky, boolean after) {
        NotifStruct.TimeScale scale;
        if (timeScale != null) {
            switch (timeScale) {
                case "Seconds":
                    scale = NotifStruct.TimeScale.SECONDS;
                    break;
                case "Minutes":
                    scale = NotifStruct.TimeScale.MINUTES;
                    break;
                case "Hours":
                    scale = NotifStruct.TimeScale.HOURS;
                    break;
                case "Days":
                    scale = NotifStruct.TimeScale.DAYS;
                    break;
                case "Weeks":
                    scale = NotifStruct.TimeScale.WEEKS;
                    break;
                case "Months":
                    scale = NotifStruct.TimeScale.MONTHS;
                    break;
                default:
                    scale = NotifStruct.TimeScale.MINUTES;
                    break;
            }
            int notifId = new Random().nextInt(Integer.MAX_VALUE);
            return new NotifStruct(time, scale, sticky, after, notifId);
        } else {
            return null;
        }
    }
}
